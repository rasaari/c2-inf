#ifndef TILELAYER_H
#define TILELAYER_H

#include <vector>
#include "tile.h"

typedef std::vector<std::vector<Tile>> tiles2DVector;

class TileLayer
{
public:
    TileLayer(const int width, const int height);
private:
    tiles2DVector           _tiles;

    int                     _width;
    int                     _height;
};

#endif // TILELAYER_H
