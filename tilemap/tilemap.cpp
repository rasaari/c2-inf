#include "tilemap.h"

Tilemap::Tilemap(int width, int height) {
    _width = width;
    _height = height;
}

void
Tilemap::addLayer() {
    TileLayer layer(_width, _height);
    _layers.push_back(layer);
}
