#ifndef TILEMAP_H
#define TILEMAP_H

#include <vector>
#include "tilelayer.h"

struct TilemapFormat {
    int width;
    int heigh;
};

class Tilemap{
public:
    Tilemap(int width, int height);

    void                    addLayer();
private:
    int                     _width;
    int                     _height;

    std::vector<TileLayer>  _layers;

};

#endif // TILEMAP_H
