#include "tilelayer.h"

TileLayer::TileLayer(const int width, const int height){
    _tiles.reserve(height);

    _width = width;
    _height = height;
}
