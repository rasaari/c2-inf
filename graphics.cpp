#include "graphics.h"

bool Graphics::init() {
    if (!glfwInit()) {
        fprintf(stderr, "Failed to initialize GLFW\n");
        return false;
    }

    glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL

    // Open a window and create its OpenGL context
    _window = glfwCreateWindow( 1024, 768, "C2 Infinity Engine", NULL, NULL);

    if( _window == NULL ){
        fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
        glfwTerminate();
        return false;
    }

    glfwMakeContextCurrent(_window); // Initialize GLEW
    glewExperimental=true; // Needed in core profile

    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        return false;
    }

    // Enable depth test
    glEnable(GL_DEPTH_TEST);

    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);

    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(_window, GLFW_STICKY_KEYS, GL_TRUE);

    //_primaryShader = LoadShaders("shader.vert", "shader.frag");
    _primaryShader.load("shader.vert", "shader.frag");

    // Get a handle for our "MVP" uniform.
    // Only at initialisation time.
    _MatrixID = glGetUniformLocation(_primaryShader.getID(), "MVP");

    return true;
}

void
Graphics::prepare() {
    glClearColor(0.0f, 0.0f, 0.2f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void
Graphics::finish() {
    glfwSwapBuffers(_window);
}

void
Graphics::render(Model *model) {
    //glUseProgram(_primaryShader);
    _primaryShader.use();

    // Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
    glm::mat4 Projection = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, 100.0f);

    // Camera matrix
    _camera = glm::lookAt(
        glm::vec3(4,3,3), // Camera is at (4,3,3), in World Space
        glm::vec3(0,0,0), // and looks at the origin
        glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
    );

    // Model matrix : an identity matrix (model will be at the origin)
    //glm::mat4 Model      = glm::mat4(1.0f);  // Changes for each model !

    // Our ModelViewProjection : multiplication of our 3 matrices
    _MVP        = Projection * _camera * model->getTransform(); // Remember, matrix multiplication is the other way around

    glUniformMatrix4fv(_MatrixID, 1, GL_FALSE, &_MVP[0][0]);

    glBindVertexArray(model->getVao());
    glEnableVertexAttribArray(0);
    glDrawArrays(GL_TRIANGLES, 0, model->getVertexCount());

    // finish
    glDisableVertexAttribArray(0);
    glBindVertexArray(0);
}


GLFWwindow* Graphics::getWindow() {
    return _window;
}

Graphics& Graphics::getInstance() {
    static Graphics instance;
    return instance;
}
