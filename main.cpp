#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <cstdio>

#include "graphics.h"
#include "shaderprogram.h"
#include "batch.h"
#include "model.h"

Graphics* _graphics;

void checkError() {
    GLenum err = glGetError();
    if(err != GL_NO_ERROR) {
        printf("error: %s\n", gluErrorString(err));
    }
}

int main() {
    _graphics = &Graphics::getInstance();

    if(!_graphics->init()) {
        return -1;
    }

    printf("init\n");
    checkError();

    static const GLfloat g_vertex_buffer_data[] = {
       -1.0f, -1.0f, 0.0f,
       1.0f, -1.0f, 0.0f,
       0.0f,  1.0f, 0.0f,
    };

    Model model;
    model.load(g_vertex_buffer_data, 3);

    printf("load\n");
    checkError();

    double currentFrame, deltaTime, lastFrame;

    do{
        currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        model.rotate(0.1f * deltaTime * 60, 0.0f, 1.0f, 0.0f);

        _graphics->prepare();
        _graphics->render(&model);
        _graphics->finish();

        checkError();

        glfwPollEvents();
    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(_graphics->getWindow(), GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwWindowShouldClose(_graphics->getWindow()) == 0 );

    model.dispose();

    return 0;
}
