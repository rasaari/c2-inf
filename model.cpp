#include "model.h"
#include <cstdio>

Model::Model() {
    _vao = 0;
    _vbo = 0;
    _transform = glm::mat4(1.0f);

    _transform = glm::translate(_transform, glm::vec3(0.0f, 0.0f, 0.0f));
}

GLuint
Model::getVao() {
    return _vao;
}

GLuint
Model::getVbo() {
    return _vbo;
}

GLuint
Model::getVertexCount() {
    return _vertexCount;
}

glm::mat4
Model::getTransform() {
    return _transform;
}

void
Model::translate(float x, float y, float z) {
    _transform = glm::translate(_transform, glm::vec3(x, y, z));
}

void
Model::rotate(float angle, float x, float y, float z) {
    _transform = glm::rotate(_transform, angle, glm::vec3(x, y, z));
}

void
Model::load(const GLfloat buffer[], GLuint vertexCount) {
    // Generate vertex array object (vao)
    glGenVertexArrays(1, &_vao);

    // Bind buffer
    glBindVertexArray(_vao);

    // Generate vertex buffer object (vbo)
    glGenBuffers(1, &_vbo);

    // Bind vbo
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);

    // Put data into vbo
    //glBufferData(GL_ARRAY_BUFFER, sizeof(buffer), buffer, GL_STATIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, 4*3*vertexCount, buffer, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

    // Unbind vbo and vao
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    _vertexCount = vertexCount;
}

void
Model::dispose() {
    glDeleteVertexArrays(1, &_vao);
    glDeleteBuffers(1, &_vbo);

    _vertexCount = 0;
}
