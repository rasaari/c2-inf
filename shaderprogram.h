#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H

#include <GL/glew.h>

#include <vector>
#include <fstream>
#include <cstdio>
#include <iostream>
#include <algorithm>

class ShaderProgram
{
public:
                            ShaderProgram();
    void                    load(const char * vertex_file_path,const char * fragment_file_path);
    void                    use();
    GLuint                  getID();
private:
    GLuint                  _programID = 0;
};

#endif // SHADERPROGRAM_H
