#ifndef GRAPHICS_H
#define GRAPHICS_H

#define GLM_FORCE_RADIANS

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include <vector>
#include <fstream>
#include <cstdio>
#include <iostream>
#include <algorithm>

#include "model.h"
#include "shaderprogram.h"

class Graphics {
public:
    static Graphics& getInstance();

    bool                    init();
    void                    prepare();
    void                    finish();
    void                    render(Model *model);
    GLFWwindow*             getWindow();

private:
                            Graphics() {}
                            Graphics(Graphics const&) = delete;
    void                    operator=(Graphics const&) = delete;
    ShaderProgram           _primaryShader;

    GLFWwindow*             _window;
    glm::mat4               _MVP;
    GLuint                  _MatrixID;
    glm::mat4               _camera;
};

#endif // GRAPHICS_H
