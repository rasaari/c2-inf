#ifndef MODEL_H
#define MODEL_H

#define GLM_FORCE_RADIANS

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

class Model
{
public:
                            Model();

    void                    load(const GLfloat buffer[], GLuint vertexCount);

    GLuint                  getVao();
    GLuint                  getVbo();
    GLuint                  getVertexCount();
    glm::mat4               getTransform();

    void                    dispose();

    void                    translate(float x, float y, float z);
    void                    rotate(float angle, float x, float y, float z);

private:
    GLuint                  _vao;
    GLuint                  _vbo;
    GLuint                  _vertexCount;
    glm::mat4               _transform;
};

#endif // MODEL_H
