#ifndef BATCH_H
#define BATCH_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <vector>

class Batch
{
public:
    Batch();

    void                    begin();
    void                    end();
private:
    GLuint                  _vbo;
    GLuint                  _vao;

    std::vector<GLfloat>    _buffer;
};

#endif // BATCH_H
